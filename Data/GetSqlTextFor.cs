﻿using System;
using System.Text;
using CodeJam.Strings;
using TaxmonRTK.Exceptions;

namespace TaxmonRTK.Data
{
    public static class GetSqlTextFor
    {
        private static string delimiter = "";

        public static string Delimiter
        {
            get => delimiter ?? throw new InvalidDelimiterException("Delimiter is not set up. Future replacing operations is not allowed");
            set => delimiter = value ?? throw new ArgumentNullException(nameof(value));
        }

        private static readonly StringBuilder _copyFlowFromStdin = new StringBuilder("COPY {TABLE_NAME} from STDIN (DELIMITER '{delimiter}', NULL '') ");
        private static readonly StringBuilder _copyFlowFromStdinNull = new StringBuilder("COPY {TABLE_NAME} from STDIN (DELIMITER '{delimiter}', NULL '/N') ");
        public static string GetCopyFlowFromStdin(bool shouldReplaceDelimiter = false)
        {
            return shouldReplaceDelimiter ? _copyFlowFromStdin.Replace("{delimiter}", Delimiter).ToString() : _copyFlowFromStdin.ToString();
        }
        public static string GetCopyFlowFromStdinWithNullMarker(bool shouldReplaceDelimiter = false)
        {
            return shouldReplaceDelimiter ? _copyFlowFromStdinNull.Replace("{delimiter}", Delimiter).ToString() : _copyFlowFromStdinNull.ToString();
        }
        public static string InsertToTransferInfo()
        {
            return "INSERT INTO public.transfer_info (transfer_id, source_system_id, taxform, taxpack, packs_count, rows_count, ernam, erdat," +
                   " ertime, taxpayer, ifns, kpp, oktmo, fiscal_year,\"period\", corr_num, transfer_type, received_packs, received_rows, status_code, created_at," +
                   "updated_at, status_updated, deleted_at, main_stage_table, stage_tables, processed_at, transformation_status_code, status_text) " +
                   "VALUES(':transfer_id', 'R12', ':TAXFORM', ':TAXPACK', 1, 0, ':login', to_char(now()::timestamp,'yyyymmdd'), to_char(now()::timestamp,'HH24miss'), ':TAXPAYER', ':IFNS', ':KPP', ':OKTMO', :FISCAL_YEAR," +
                   "':PERIOD', :CORR_NUM, :transfer_type, 0, 0, :status_code,now()::timestamp, now()::timestamp, now()::timestamp,null, '','{'':stage_table''}', now()::timestamp, 0,':status_text') "
            ;
        }

        public static string InsertToTransferPackInfo()
        {
            return "INSERT INTO public.transfer_packs_info" +
                   "(transfer_id, source_system_id, taxform, pack_number, status, created_at, updated_at, deleted_at)" +
                   "VALUES(':transfer_id', 'R12', ':TAXFORM',1, :status_code,  now(), now(), null) ON CONFLICT DO NOTHING"
                ;
        }

        public static string InsertToValidationLog()
        {
            return "INSERT INTO public.validation_log" +
                   "(transfer_id, source_system_id, pack_number, step, status_code, status_text, created_at," +
                   " updated_at, output_order, taxform)" +
                   "VALUES(':transfer_id', 'R12', 1, :step, ':status_code', ':status_text', now(), now(), 0,':TAXFORM')"
                ;
        }


        public static string UpdateTransferInfo()
        {
            return "UPDATE public.transfer_info SET  rows_count=:count,  received_packs=1, received_rows=:count, status_code=2, status_updated=now()," +
                   " status_text='Загружено' WHERE transfer_id = ':transfer_id' AND source_system_id = 'R12' AND taxform = ':TAXFORM' AND IFNS =':IFNS'"
                ;
        }
        public static string UpdateTransferInfoError()
        {
            return "UPDATE public.transfer_info SET  rows_count=:count,  received_packs=1, received_rows=:count, status_code=5, status_updated=now()," +
                   " status_text='Ошибка: данные не получены, загрузка не удалась' WHERE transfer_id = ':transfer_id' AND source_system_id = 'R12'"
                ;
        }

        public static string InsertToReportVers()
        {
            return "INSERT INTO stage.report_vers" +
                   "(org_unit_code,tax_authority_code,kpp,oktmo,fiscal_year,\"period\",corr_num,taxform_code,version_reg," +
                   "status,line_count,source_system,id_transfer,is_blocked,is_visible,is_enriched,uploaded_by,upload_time,edited_by,edit_time,row_ver)" +
                   "VALUES(':TAXPAYER', ':ifns', ':kpp', ':oktmo', :FISCAL_YEAR, ':PERIOD', :CORR_NUM, ':TAXFORM', :version_reg," +
                   "':status', :count, 'R12', ':transfer_id', false, true, false, ':login', now(), ':login', now(),0) ON CONFLICT DO NOTHING"
                ;
        }

        public static string UpdateReportVers()
        {
            return "UPDATE stage.report_vers " +
                   "SET version_reg = 1, copied_from_org_unit = null, copied_from_tax_authority = null, copied_from_kpp = null, copied_from_oktmo = null," +
                   "status = 'Загружено', line_count =:count, source_system='R12', " +
                   "id_transfer= ':transfer_id', is_blocked=false,  is_visible=true, is_enriched=false, uploaded_by=':login', upload_time=now(), edited_by = ':login',edit_time =now(), row_ver=0 " +
                   "WHERE org_unit_code = ':TAXPAYER'  AND  fiscal_year =:FISCAL_YEAR AND  \"period\"=':PERIOD' AND  corr_num=:CORR_NUM AND  taxform_code=':TAXFORM' AND  tax_authority_code=':IFNS'"
            ;
        }

        public static string SelectReportVers()
        {
            return "SELECT  coalesce(max(version_reg)+1,1)" +
                   "FROM stage.report_vers where org_unit_code = ':TAXPAYER' and tax_authority_code = ':ifns' and kpp = ':kpp'" +
                   "and oktmo = ':oktmo' and fiscal_year =:FISCAL_YEAR and \"period\"=':PERIOD' " +
                   "and corr_num =:CORR_NUM and taxform_code= ':TAXFORM'";
        }
    }
}