using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using FlowCommon.config;
using FlowCommon.core;
using FlowCommon.core.contract;
using FlowCommon.core.Contracts;
using FlowCommon.Exceptions;
using Grpc.Net.Client;
using Npgsql;
using Oracle.ManagedDataAccess.Client;
using Serilog;
using TaxmonRTK.Data;

//using grpcClient;
//using taxmonGrpc;

namespace TaxmonRTK;

public class TaxmonOracleR12Flow : ImportFlowBase
{
    private const bool REPLACE_DELIMITER = true;

    private const int PACKAGE_SIZE = 250000;

    private const string NULL_MARKER_FOR_COPY_FROM = "/N";
    
    private static readonly ILogger Log = Utils.getLogger<TaxmonOracleR12Flow>();

    public static Dictionary<string, string> ResultOperations = new();

    private static StringBuilder LogProtocol = new();

    private readonly vnm_r12oracle_import_service _config;

    private Dictionary<string, connection> connections; //id как ключ

    private Dictionary<string, grpc> grpc_service; // id как ключ

    private Dictionary<string, string> parameters; //name_parameter как ключ

    private Dictionary<string, service> services; //name как ключ

    private string[] STATUS = new string[3] { "processing", " Done", "Error" };

    public Dictionary<string, flow> streams; // name как ключ

    public TaxmonOracleR12Flow(vnm_r12oracle_import_service inputConfig)
    {
        _config = inputConfig;
        LogProtocol = new StringBuilder();
    }

    private static void _Log_Info_With_Protocol(string infoMessage)
    {
        Log.Information(infoMessage);
        LogProtocol.AppendLine(infoMessage);
    }

    private static void _Log_Error_to_Protocol(Exception e, string errorMessage)
    {
        Log.Error(e, errorMessage);
        LogProtocol.AppendLine($"ERROR: {errorMessage}");
    }


    private void XmlConfig(string FileName)
    {
        // LogProtocol = new StringBuilder();
        // _Log_Info_With_Protocol($"чтение файла конфигурации {Environment.CurrentDirectory + "\\" + FileName}");
        //parameters = new Dictionary<string, string>();
        // Console.WriteLine($"Current directory is '{Environment.CurrentDirectory}'");
        var connect = new connection();
        connections = new Dictionary<string, connection>();
        var srv = new service();
        services = new Dictionary<string, service>();
        var flow = new flow();
        streams = new Dictionary<string, flow>();
        var grpc = new grpc();
        grpc_service = new Dictionary<string, grpc>();

        var connectionArray = _config.connections.Select(t => t).ToArray();
        foreach (var connectionItem in connectionArray)
        {
            connect.id = connectionItem.id;
            connect.driverClassname = connectionItem.driverClassName;
            connect.type = connectionItem.type;
            connect.auth_token = connectionItem.auth_token;
            connect.location = connectionItem.location;

            connections.Add(connect.id, connect);
            Log.Information("connection {0}: {1}", connect.id, connect.location);
        }

        var a = 2 + 2;
        var flowArray = _config.flows.Select(t => t).ToArray();
        foreach (var flowItem in flowArray)
        {
            flow.name = flowItem.name;
            flow.type_flow = flowItem.type_flow;
            flow.taxpack = flowItem.taxpack;
            flow.taxform = flowItem.taxform;
            flow.module = flowItem.module;
            flow.queue_connection = flowItem.queue_connection;
            flow.queue_table = flowItem.queue_table;
            flow.source_connection = flowItem.source_connection;
            flow.target_connection = flowItem.target_connection;
            flow.calculation_delta_val = flowItem.calculation_delta_val;
            flow.source_query_text = flowItem.source_query_text;
            flow.target_table = flowItem.target_table;
            flow.update_target = flowItem.update_target;

            streams.Add(flowItem.taxpack + "_" + flowItem.name, flow);
            // Log.Information("Flow {0}: type: {1} target table: {2}", flow.taxpack + "_" + flow.name, flow.type_flow, flow.target_table);
            _Log_Info_With_Protocol($"Flow {flow.taxpack}_{flow.name}: type: {flow.type_flow} target table: {flow.target_table}");        }
    }

    private Dictionary<string, connection> GetListConnections()
    {
        return connections;
    }

    private Dictionary<string, connection> GetListConnections(string driver)
    {
        var ListConnection = new Dictionary<string, connection>();
        foreach (var param in connections)
            if (param.Value.driverClassname == driver)
                ListConnection.Add(param.Key, param.Value);

        return ListConnection;
    }

    private Dictionary<string, grpc> GetListGrpcConnection(string type)
    {
        var ListGrpcConnection = new Dictionary<string, grpc>();
        foreach (var param in grpc_service)
            if (param.Value.type == type)
                ListGrpcConnection.Add(param.Key, param.Value);

        return ListGrpcConnection;
    }

    private void SetParameters(Dictionary<string, string> param)
    {
        parameters = param;
    }

    private OracleConnection GetConnectionOracle()
    {
        var connect_params = GetListConnections("oracle");
        var myConnectionString = "";
        var key = "";
        var host = "";
        var port = "";
        var service_name = "";
        foreach (var params_connect in connect_params) key = params_connect.Key;

        myConnectionString = connect_params[key].location;
        host = connect_params[key].host;
        port = connect_params[key].port;
        service_name = connect_params[key].service_name;
        var newBytes = Convert.FromBase64String(connect_params[key].auth_token);
        var auth_token = Encoding.UTF8.GetString(newBytes, 0, newBytes.Length);
        myConnectionString = myConnectionString.Replace("{pwd}", auth_token);
        var myConnection = new OracleConnection();
        myConnection.ServiceName = service_name;
        myConnection.ConnectionString = myConnectionString;
        return myConnection;
    }

    private GrpcChannel GetGPRCGrpcChannel(string gprc_name)
    {
        var connect_params = GetListGrpcConnection(gprc_name);
        var addres = "";
        var key = "";
        foreach (var params_connect in connect_params) key = params_connect.Key;

        addres = connect_params[key].location;
        //byte[] newBytes = Convert.FromBase64String(connect_params[key].auth_token);
        //String auth_token = BitConverter.ToString(newBytes);
        var channel = GrpcChannel.ForAddress(addres);
        return channel;
    }

    private string GetConnectionStage()
    {
        var connect_params = GetListConnections("postgresql");
        var myConnectionString = "";
        var key = "";
        foreach (var params_connect in connect_params)
            if (params_connect.Key.ToUpper() == "VNM_DEFAULT")
                key = params_connect.Key;

        myConnectionString = connect_params[key].location;
        var newBytes = Convert.FromBase64String(connect_params[key].auth_token);
        var auth_token = Encoding.UTF8.GetString(newBytes, 0, newBytes.Length);
        myConnectionString = myConnectionString.Replace("{pwd}", auth_token);
        return myConnectionString;
    }

    private string GetConnectionMonitor()
    {
        var connect_params = GetListConnections("postgresql");
        var myConnectionString = "";
        var key = "";
        foreach (var params_connect in connect_params)
            if (params_connect.Key.ToUpper() == "TECHMONITOR")
                key = params_connect.Key;

        myConnectionString = connect_params[key].location;
        var newBytes = Convert.FromBase64String(connect_params[key].auth_token);
        var auth_token = Encoding.UTF8.GetString(newBytes, 0, newBytes.Length);
        myConnectionString = myConnectionString.Replace("{pwd}", auth_token);
        return myConnectionString;
    }

    private NpgsqlConnection GetConnectionQueue()
    {
        var connect_params = GetListConnections("postgresql");
        var myConnectionString = "";
        var key = "";
        foreach (var params_connect in connect_params)
            if (params_connect.Key == "vnm_queue")
                key = params_connect.Key;

        myConnectionString = connect_params[key].location;
        var newBytes = Convert.FromBase64String(connect_params[key].auth_token);
        var auth_token = BitConverter.ToString(newBytes);
        myConnectionString = myConnectionString.Replace("{pwd}", auth_token);
        var myConnection = new NpgsqlConnection(myConnectionString);
        return myConnection;
    }

    private string GetTransferID(string name, string form)
    {
        var dt = DateTime.Now;
        if (form.Length > 17)
            form = form.Substring(0, 17).Replace("_", "");
        else
            form = form.Replace("_", "");

        var transfer_id = form + "_" + dt.ToString("yyyyMMddHHmmss");
        return transfer_id;
    }

    private void UpdateStatusLog(string queue_table, int id, string transfer_id, string status,
        NpgsqlConnection queue_conn)
    {
        var sql = "UPDATE " + queue_table +
                  " SET status = ':status', transfer_id = ':transfer_id', update_at = now() where id = ".Replace(
                      ":transfer_id", transfer_id) + Convert.ToInt64(1d);
        sql = sql.Replace(":status", status);
        queue_conn.Open();
        var cmd = new NpgsqlCommand(sql, queue_conn);
        cmd.ExecuteNonQuery();
        queue_conn.Close();
    }

    private List<flow> GetFlow(string Taxpack)
    {
        var flw = new List<flow>();

        foreach (var stream in streams)
            if (stream.Value.taxpack == Taxpack)
                flw.Add(stream.Value);


        return flw;
    }

    private int GetReportVers(NpgsqlConnection pg_conn)
    {
        var vers = 0;


        return vers;
    }

    // private string SQL_COPY_flw = "COPY {table_name} from STDIN (DELIMITER '{delimiter}', NULL '') ";

    //@tood(nvv): рассмотри простой переход на стейтмент-пг
    private string SetparamQuery(string sql)
    {
        foreach (var filtr in parameters)
        {
            var key = filtr.Key;
            var param = ":" + filtr.Key;
            var val = filtr.Value;
            sql = sql.Replace(param, val);
        }

        return sql;
    }

    private void SetErrorStatusTransferInfo(string transfer_id, NpgsqlConnection pg_mon_conn)
    {
        pg_mon_conn.Open();
        var sql_update = GetSqlTextFor.UpdateTransferInfoError().Replace(":transfer_id", transfer_id);
        var cmd_upd = new NpgsqlCommand(sql_update, pg_mon_conn);
        cmd_upd.ExecuteNonQuery();
        cmd_upd.Dispose();
        pg_mon_conn.Close();
    }

    public Dictionary<string, string> RunETL(Dictionary<string, string> array_params)
    {
        string status_etl;
        var new_data = true;

        try
        {
            GetSqlTextFor.Delimiter = ConfigReader.getTransferConfig().preferences.delimiter; 
            XmlConfig("rtk_mapping.xml");
            parameters = array_params;
            var create_date = DateTime.Now.ToString("yyyMMdd");
            var SQL_COPY_flw = GetSqlTextFor.GetCopyFlowFromStdin(REPLACE_DELIMITER).ToUpper();

            var infoMessage = $"parameters: {string.Join(";", parameters.Select(x => $"{x.Key}={x.Value}"))}";
            // Log.Information(infoMessage);
            _Log_Info_With_Protocol(infoMessage);
            var rep_vers = "";
            var target_table = "";
            var transfer_id = "";
            var SQL = "";
            string value_params = "";
            var Tax_pack = "";
            int fiscal_year;
            Action<string> forms;

            var period = parameters["PERIOD"];
            var login_user = parameters["LOGIN"];
            parameters.Add("TAXPACK", parameters["TAX_PACK"]);
            if (parameters.TryGetValue("TAX_PACK", out value_params))
            {
                Tax_pack = parameters["TAX_PACK"];
                if (Tax_pack != TAXPACK.TRANSACTIONAL && Tax_pack != TAXPACK.HANDBOOK)
                    fiscal_year = int.Parse(parameters["FISCAL_YEAR"]);
            }
            else
            {
                Tax_pack = parameters["TYPE_FLOW"];
            }

            var f = GetFlow(Tax_pack);

            // Log.Information("count streams to download: {0}", f.Count);
            _Log_Info_With_Protocol($"Count streams to download: {f.Count}");
            //формируется подключение к oracle
            var ora_conn = GetConnectionOracle();
            var com = ora_conn.CreateCommand();
            //подключение к Витрине и мониторингу
            var stage_conn = GetConnectionStage();
            var monitor_conn = GetConnectionMonitor();
            var pg_conn = new NpgsqlConnection(stage_conn);
            var pg_mon_conn = new NpgsqlConnection(monitor_conn);
            // Log.Information("Чтение и выполнение потоков");
            _Log_Info_With_Protocol("Чтение и выполнение потоков");
            foreach (var flw in f)
            {
                //*************************************************************************************************************
                if (flw.type_flow == TYPE_FLOW.REPORTING_DATA && Tax_pack == flw.taxpack)
                {
                    // Log.Information($"Чтение и выполнение потока отчетных данных type_flow = 2: {flw.name}");
                    _Log_Info_With_Protocol($"Чтение и выполнение потока отчетных данных type_flow = 2: {flw.name}");
                    var SQL_delta = flw.calculation_delta_val;
                    var get_list_forms = flw.taxform;
                    var value = "";
                    var ifns = "";
                    var kpp = "";
                    var oktmo = "";
                    var periods = "";
                    var new_form = false;
                    target_table = flw.target_table;
                    var SQL_del = flw.update_target;
                    var post_SQL = flw.update_target;
                    SQL = flw.source_query_text;
                    // var SQL_COPY = SQL_COPY_flw;
                    var SQL_COPY = GetSqlTextFor.GetCopyFlowFromStdinWithNullMarker(REPLACE_DELIMITER);
                    SQL_COPY = SQL_COPY.Replace("{TABLE_NAME}", target_table);
                    // Log.Information($"SQL_COPY {SQL_COPY}");
                    _Log_Info_With_Protocol($"SQL_COPY {SQL_COPY}");
                    var SQL_transfer_info_ins = GetSqlTextFor.InsertToTransferInfo();
                    var SQL_transfer_pack_ins = GetSqlTextFor.InsertToTransferPackInfo();
                    var SQL_report_vers_ins = GetSqlTextFor.InsertToReportVers();
                    var SQL_transfer_info_upd = GetSqlTextFor.UpdateTransferInfo();
                    var SQL_validation_ins = GetSqlTextFor.InsertToValidationLog();
                    var SQL_validation_ins5 = "";
                    var SQL_validation_ins6 = "";
                    var SQL_validation_ins8 = "";
                    var SQL_get_report_ver = "";
                    var SQL_update_report_ver = GetSqlTextFor.UpdateReportVers();
                    string[] array_forms = null;
                    string[] array_ifns = null;
                    string[] array_kpp = null;
                    string[] array_oktmo = null;
                    var list_forms = new List<string>();
                    var list_ifns = new List<string>();

                    get_list_forms = SetparamQuery(get_list_forms);

                    if (flw.taxpack == "D01" || flw.taxpack == "D02")
                    {
                        SQL_transfer_info_ins = SQL_transfer_info_ins;
                        SQL_transfer_info_upd = SQL_transfer_info_upd.Replace("AND IFNS =':IFNS'", "");
                    }

                    SQL_validation_ins = SetparamQuery(SQL_validation_ins).Replace("' '", "''");
                    SQL_transfer_pack_ins = SetparamQuery(SQL_transfer_pack_ins).Replace("' '", "''");
                    SQL_get_report_ver = SetparamQuery(GetSqlTextFor.SelectReportVers()).Replace("' '", "''");
                    SQL_report_vers_ins = SetparamQuery(SQL_report_vers_ins).Replace("' '", "''");
                    SQL_update_report_ver = SetparamQuery(SQL_update_report_ver).Replace("' '", "''");
                    SQL_del = SetparamQuery(SQL_del);
                    // получаем последнюю дату загруженных форм по пакету отчетности                   
                    _Log_Info_With_Protocol("Получаем последнюю дату загруженных форм по пакету отчетности");
                    pg_conn.Open();
                    pg_mon_conn.Open();
                    if (parameters["IFNS"] != " ") SQL_delta = SQL_delta + " AND IFNS =':IFNS'";
                    SQL_delta = SetparamQuery(SQL_delta);
                    _Log_Info_With_Protocol($"Запрос: {SQL_delta}");
                    var pg_command = new NpgsqlCommand(SQL_delta, pg_conn);
                    var delta_param = pg_command.ExecuteScalar().ToString();
                    _Log_Info_With_Protocol(delta_param);
                    get_list_forms = get_list_forms.Replace(":date", delta_param);
                    ora_conn.Open();
                    com = new OracleCommand(get_list_forms, ora_conn);

                    // получаем перечень актуальных форм и периодов
                    _Log_Info_With_Protocol("Получаем перечень актуальных форм и периодов ");
                    _Log_Info_With_Protocol($"Запрос {get_list_forms}");
                    try
                    {
                        using (var oracleReader = com.ExecuteReader())
                        {
                            while (oracleReader.Read())
                                if (oracleReader.IsDBNull(0))
                                {
                                    _Log_Info_With_Protocol($"Новые формы по пакету {flw.taxpack} не найдены");
                                    new_data = true;
                                    new_form = false;
                                }
                                else
                                {
                                    new_form = true;
                                    value = (string)oracleReader.GetOracleString(0);
                                    periods = (string)oracleReader.GetOracleString(2);
                                    ifns = (string)oracleReader.GetOracleString(3);
                                    kpp = (string)oracleReader.GetOracleString(4);
                                    oktmo = (string)oracleReader.GetOracleString(5);
                                    array_forms = value.Split(';');
                                    array_ifns = ifns.Split(',');
                                    array_kpp = kpp.Split(',');
                                    array_oktmo = oktmo.Split(',');
                                    list_forms = array_forms.ToList();
                                    list_ifns = array_ifns.ToList();
                                }
                        }
                    }
                    catch (Exception e)
                    {
                        _Log_Info_With_Protocol($"Новые формы по пакету {flw.taxpack} не найдены");
                        continue;
                    }
                    SQL_transfer_info_ins = SetparamQuery(SQL_transfer_info_ins.Replace(":login",login_user).Replace("':IFNS'",array_ifns[0]).Replace("':KPP'",array_kpp[0])).Replace("' '", "''");


                    //обработка форм
                    if (new_form)
                    {
                        _Log_Info_With_Protocol($"Формы по пакету {array_forms} для загрузки");
                        _Log_Info_With_Protocol("Обработка форм");
                        for (var ii = list_forms.Count - 1; ii >= 0; ii--)
                        {
                            SQL = flw.source_query_text;
                            var forma = list_forms[ii];
                            transfer_id = GetTransferID(flw.taxpack, forma);
                            _Log_Info_With_Protocol($"transfer_id =  {transfer_id}");
                            _Log_Info_With_Protocol($"Форма {forma}");

                            if (flw.taxpack != "D01" && flw.taxpack != "D02" /*&& flw.taxpack != "D03" */)
                            {
                                for (var jjj = list_ifns.Count - 1; jjj >= 0; jjj--)
                                {
                                    var id_transfer = list_ifns[jjj].Replace("'", "") + transfer_id;
                                    var codeifns = list_ifns[jjj];

                                    SQL_get_report_ver = SetparamQuery(GetSqlTextFor.SelectReportVers())
                                        .Replace("' '", "''");
                                    SQL_get_report_ver = SQL_get_report_ver.Replace(":TAXFORM", forma)
                                        .Replace("':ifns'", codeifns).Replace("':kpp'", array_kpp[0].Trim())
                                        .Replace(":oktmo", parameters["OKTMO"].Trim());
                                    SQL_get_report_ver = SetparamQuery(SQL_get_report_ver).Replace("' '", "''");
                                    var command_get_vers = new NpgsqlCommand(SQL_get_report_ver, pg_conn);
                                    var dataReader_vers = command_get_vers.ExecuteScalar();
                                    rep_vers = dataReader_vers.ToString();
                                    if (rep_vers == "1")
                                    {
                                        //если определена версия 1, то добовляем новую запись
                                        _Log_Info_With_Protocol(
                                            $"запись первой версиии для ИФНС   {codeifns}, {SQL_get_report_ver}");
                                        SQL_report_vers_ins = SQL_report_vers_ins.Replace(":TAXFORM", forma)
                                            .Replace(":version_reg", "1").Replace(":status", "").Replace(":count", "0")
                                            .Replace(":transfer_id", id_transfer).Replace("':ifns'", codeifns)
                                            .Replace("':kpp'", array_kpp[0].Trim())
                                            .Replace(":oktmo", parameters["OKTMO"].Trim())
                                            .Replace(":login", login_user);
                                        SQL_report_vers_ins = SetparamQuery(SQL_report_vers_ins).Replace("' '", "''");
                                        var command_ins_vers = new NpgsqlCommand(SQL_report_vers_ins, pg_conn);
                                        var dataReader_ins_vers = command_ins_vers.ExecuteNonQuery();
                                        command_ins_vers.Dispose();
                                        SQL_report_vers_ins = GetSqlTextFor.InsertToReportVers();
                                    }
                                    else
                                    {
                                        _Log_Info_With_Protocol(
                                            $"Запись первой версиии для ИФНС  существует  {forma}, {codeifns}");
                                    }

                                    var sql = SetparamQuery(GetSqlTextFor.InsertToTransferInfo()
                                        .Replace("':IFNS'", codeifns));
                                    SQL_transfer_info_ins = sql.Replace(":transfer_id", id_transfer)
                                        .Replace(":TAXFORM", forma).Replace(":transfer_type", "2")
                                        .Replace(":status_code", "1").Replace(":stage_table", target_table)
                                        .Replace(":status_text", "В процессе загрузки")
                                        .Replace(":login", login_user)
                                        .Replace(":transfer_id", id_transfer);
                                    sql = SetparamQuery(GetSqlTextFor.InsertToTransferPackInfo());
                                    SQL_transfer_pack_ins = sql.Replace(":transfer_id", id_transfer)
                                        .Replace(":TAXFORM", forma).Replace(":transfer_type", "2")
                                        .Replace(":status_code", "1").Replace(":stage_table", target_table)
                                        .Replace(":status_text", "В процессе загрузки")
                                        .Replace(":transfer_id", id_transfer);
                                    SQL_validation_ins = SQL_validation_ins.Replace(":transfer_id", id_transfer)
                                        .Replace(":TAXFORM", forma).Replace(":step", "6")
                                        .Replace(":status_code", "success").Replace(":status_text", "");
                                    var SQL_source = SQL.Replace("':IFNS'", codeifns);
                                    SQL_source = SetparamQuery(SQL_source).Replace(":form", forma);
                                    if (SQL_source.IndexOf(" IN ") > 0)
                                        SQL_source = SQL_source.Replace(":period", periods).Replace(":version_reg", "1")
                                            .Replace(":date", create_date);
                                    else
                                        SQL_source = SQL_source.Replace(":period", parameters["PERIOD"])
                                            .Replace(":version_reg", "1").Replace(":date", create_date)
                                            .Replace(":login", login_user);
                                    //записываем transfer_info                       
                                    var command_ins = new NpgsqlCommand(SQL_transfer_info_ins, pg_mon_conn);
                                    command_ins.ExecuteNonQuery();
                                    _Log_Info_With_Protocol("Запись transfer_info");
                                    command_ins.Dispose();
                                    var count_rows = 0;
                                    _Log_Info_With_Protocol("Запуск процесса переноса данных");
                                    // SQL_get_report_ver = SQL_get_report_ver.Replace(":TAXFORM", forms).Replace("':ifns'", array_ifns[0]).Replace("':kpp'", array_kpp[0]).Replace("':oktmo'", array_oktmo[0]);
                                    // SQL_report_vers_ins = SQL_report_vers_ins.Replace(":TAXFORM", forma).Replace("':ifns'", codeifns).Replace("':kpp'", array_kpp[0]).Replace("':oktmo'", array_oktmo[0]);
                                    SQL_source = SQL_source + "AND IFNS =" + list_ifns[jjj];
                                    SQL_source = SQL_source.Replace(":login", login_user);
                                    com = new OracleCommand(SQL_source, ora_conn);
                                    _Log_Info_With_Protocol(SQL_source);
                                    try
                                    {
                                        using (var oracleReader = com.ExecuteReader())
                                        {
                                            var filed_count = oracleReader.FieldCount;
                                            using (var writer = pg_conn.BeginTextImport(SQL_COPY))
                                            {
                                                while (oracleReader.Read())
                                                {
                                                    var row = string.Join(GetSqlTextFor.Delimiter, GetProcessedColumnValuesFromOracleToPg(oracleReader))
                                                        .Replace("\r\n", " ").Replace("\n", " ")
                                                        .Replace("\r", " ").Replace(":version_reg", "1")
                                                        .Replace(":transfer_id", id_transfer)
                                                        .Replace(":date", create_date)
                                                        .Replace("\\", "/")
                                                    ;
                                                    writer.WriteLine(row);
                                                    count_rows++;
                                                }

                                                writer.Close();
                                                _Log_Info_With_Protocol($"Количество обработаных записей {count_rows}");
                                                SQL_transfer_info_upd = GetSqlTextFor.UpdateTransferInfo()
                                                    .Replace(":transfer_id", id_transfer).Replace(":TAXFORM", forma)
                                                    .Replace(":status_code", "8").Replace(":status_text", "Загружено")
                                                    .Replace(":count", count_rows.ToString())
                                                    .Replace("status_code=2", "status_code=8")
                                                    .Replace("':IFNS'", list_ifns[jjj]);
                                                SQL_transfer_pack_ins = SQL_transfer_pack_ins
                                                    .Replace(":transfer_id", id_transfer).Replace(":TAXFORM", forma)
                                                    .Replace(":status_code", "1");
                                                SQL_validation_ins5 = SQL_validation_ins
                                                    .Replace(":transfer_id", id_transfer).Replace(":TAXFORM", forma)
                                                    .Replace(":step", "5").Replace(":status_code", "success")
                                                    .Replace(":status_text", "");
                                                SQL_validation_ins6 = SQL_validation_ins
                                                    .Replace(":transfer_id", id_transfer).Replace(":TAXFORM", forma)
                                                    .Replace(":step", "6").Replace(":status_code", "success")
                                                    .Replace(":status_text", "");
                                                SQL_validation_ins8 = SQL_validation_ins
                                                    .Replace(":transfer_id", id_transfer).Replace(":TAXFORM", forma)
                                                    .Replace(":step", "8").Replace(":status_code", "success")
                                                    .Replace(":status_text", "");
                                                SQL_report_vers_ins = SQL_report_vers_ins
                                                    .Replace(":transfer_id", id_transfer).Replace(":TAXFORM", forma)
                                                    .Replace(":version_reg", rep_vers).Replace(":login", login_user)
                                                    .Replace(":status", "Загружено")
                                                    .Replace(":count", count_rows.ToString())
                                                    .Replace("':ifns'", list_ifns[jjj]).Replace("':kpp'", array_kpp[0])
                                                    .Replace("':oktmo'", array_oktmo[0]);
                                                //обновление report_vers
                                                var maybeUpdateReportVerSql = SetparamQuery(GetSqlTextFor.UpdateReportVers().Replace(":TAXFORM", forma).Replace(":login", login_user).Replace(":count", count_rows.ToString()).Replace(":transfer_id", transfer_id)
                                                    .Replace("':ifns'", list_ifns[jjj]));
                                                
                                                SQL_update_report_ver =  maybeUpdateReportVerSql;
                                                _Log_Info_With_Protocol($"Обновляем запись первой версиии   {SQL_get_report_ver}");
                                                pg_conn.Close();
                                                pg_conn.Open();
                                                var command_upd_vers = new NpgsqlCommand(SQL_update_report_ver, pg_conn);
                                                command_upd_vers.ExecuteNonQuery();
                                                command_upd_vers.Dispose();
                                                _Log_Info_With_Protocol("Обновление таблиц тех монитора");
                                                var command = new NpgsqlCommand(SQL_transfer_info_upd, pg_mon_conn);
                                                command.ExecuteNonQuery();
                                                //command.CommandText = SQL_transfer_pack_ins;
                                                //command.ExecuteNonQuery();
                                                command.CommandText = SQL_validation_ins5;
                                                command.ExecuteNonQuery();
                                                command.CommandText = SQL_validation_ins6;
                                                command.ExecuteNonQuery();
                                                // command.CommandText = SQL_report_vers_ins;
                                                //command.ExecuteNonQuery();
                                                command.CommandText = SQL_validation_ins8;
                                                command.ExecuteNonQuery();
                                                command.Dispose();
                                                pg_conn.Close();
                                                pg_conn.Open();
                                                _Log_Info_With_Protocol(
                                                    $"Удаление предыдущего среза transfer_id <>  {id_transfer}");
                                                var post_SQL_ifns = post_SQL + "AND IFNS =" + list_ifns[jjj];
                                                post_SQL_ifns = SetparamQuery(post_SQL_ifns)
                                                    .Replace(":transfer_id", id_transfer).Replace(":TAXFORM", forma);
                                                _Log_Info_With_Protocol(post_SQL);
                                                var comm_post_sql = new NpgsqlCommand(post_SQL_ifns, pg_conn);
                                                // var dataReader_post_sql = command_post_sql.ExecuteNonQueryAsync();
                                                comm_post_sql.ExecuteNonQuery();
                                                pg_conn.Close();
                                                pg_conn.Open();
                                            }
                                        }
                                    }
                                    catch (Exception e)
                                    {
                                        var SQL_transfer_info_upd_error = SQL_transfer_info_upd;
                                        SQL_transfer_info_upd_error = GetSqlTextFor.UpdateTransferInfo()
                                            .Replace(":transfer_id", id_transfer).Replace(":TAXFORM", forma)
                                            .Replace(":status_code", "5").Replace(":status_text", "Ошибка")
                                            .Replace("Загружено", "Ошибка: данные не получены, загрузка не удалась")
                                            .Replace(":count", count_rows.ToString())
                                            .Replace("':IFNS'", codeifns)
                                            .Replace("status_code=2", "status_code=5");
                                        var command = new NpgsqlCommand(SQL_transfer_info_upd_error, pg_mon_conn);
                                        command.ExecuteNonQuery();
                                        pg_conn.Close();
                                        pg_conn.Open();
                                    }
                                }

                                ;
                            }
                            else
                            {
                                // SQL_get_report_ver = SQL_get_report_vers;
                                var codeifns = list_ifns[0];
                                var code_kpp = array_kpp[0];
                                var id_transfer = transfer_id;
                                var get_report_ver = SQL_get_report_ver;
                                get_report_ver = get_report_ver.Replace(":TAXFORM", forma)
                                    .Replace("tax_authority_code = ':ifns'", "tax_authority_code IN (':ifns')")
                                    .Replace("':ifns'", array_ifns[0].Trim()).Replace("':kpp'", array_kpp[0].Trim())
                                    .Replace("':oktmo'", "''");
                                get_report_ver = SetparamQuery(get_report_ver).Replace("' '", "''");
                                var command_get_vers = new NpgsqlCommand(get_report_ver, pg_conn);
                                var dataReader_vers = command_get_vers.ExecuteScalar();
                                rep_vers = dataReader_vers.ToString();
                                if (rep_vers == "1")
                                {
                                    //если определена версия 1, то добовляем новую запись
                                    _Log_Info_With_Protocol(
                                        $"запись первой версиии для формы   {forma}, {flw.taxpack}, {SQL_get_report_ver}");
                                    SQL_report_vers_ins = GetSqlTextFor.InsertToReportVers();
                                    var report_vers_ins = SQL_report_vers_ins;
                                    report_vers_ins = report_vers_ins.Replace(":TAXFORM", forma)
                                        .Replace(":version_reg", "1").Replace(":status", "").Replace(":count", "0")
                                        .Replace(":transfer_id", transfer_id).Replace("':ifns'", array_ifns[0].Trim())
                                        .Replace("':kpp'", array_kpp[0].Trim()).Replace("':oktmo'", "''")
                                        .Replace(":login", login_user);
                                    report_vers_ins = SetparamQuery(report_vers_ins).Replace("' '", "''");
                                    var command_ins_vers = new NpgsqlCommand(report_vers_ins, pg_conn);
                                    var dataReader_ins_vers = command_ins_vers.ExecuteNonQuery();
                                    command_ins_vers.Dispose();
                                    SQL_report_vers_ins = GetSqlTextFor.InsertToReportVers();
                                }
                                else
                                {
                                    _Log_Info_With_Protocol(
                                        $"запись первой версиии для формы  существует  {forma}, {flw.taxpack}");
                                }

                                SQL = SetparamQuery(SQL.Replace("':IFNS'", codeifns)).Replace(":form", forma)
                                    .Replace(":transfer_id", transfer_id).Replace(":login", login_user);
                                if (SQL.IndexOf(" IN ") > 0)
                                    SQL = SQL.Replace(":period", periods).Replace(":version_reg", "1")
                                        .Replace(":date", create_date);
                                else
                                    SQL = SQL.Replace(":period", parameters["PERIOD"]).Replace(":version_reg", "1")
                                        .Replace(":date", create_date).Replace(":login", login_user);
                                //записываем transfer_info  
                                var transfer_info_ins = SQL_transfer_info_ins;
                                transfer_info_ins = transfer_info_ins.Replace(":TAXFORM", forma)
                                    .Replace(":status_code", "1").Replace(":status_text", "В процессе загрузки")
                                    .Replace(":stage_table", flw.target_table).Replace(":transfer_type", "2")
                                    .Replace(":transfer_id", transfer_id);
                                var command_ins = new NpgsqlCommand(transfer_info_ins, pg_mon_conn);
                                command_ins.ExecuteNonQuery();
                                _Log_Info_With_Protocol("Запись transfer_info");
                                command_ins.Dispose();
                                var count_rows = 0;
                                _Log_Info_With_Protocol("Запуск процесса переноса данных");
                                SQL = SQL + "AND IFNS =" + list_ifns[0];
                                com = new OracleCommand(SQL, ora_conn);
                                _Log_Info_With_Protocol(SQL);
                                try
                                {
                                    using (var oracleReader = com.ExecuteReader())
                                    {
                                        var filed_count = oracleReader.FieldCount;
                                        using (var writer = pg_conn.BeginTextImport(SQL_COPY))
                                        {
                                            try
                                            {
                                                while (oracleReader.Read())
                                                {
                                                    var row = string.Join(GetSqlTextFor.Delimiter, GetProcessedColumnValuesFromOracleToPg(oracleReader))
                                                        .Replace("\r\n", " ").Replace("\n", " ")
                                                        .Replace("\r", " ").Replace(":version_reg", "1")
                                                        .Replace(":transfer_id", id_transfer)
                                                        .Replace(":date", create_date)
                                                        .Replace("\\", "/")
                                                    ;
                                                    
                                                    writer.WriteLine(row);
                                                    count_rows++;
                                                }

                                                writer.Close();
                                            }
                                            catch (Exception e)
                                            {
                                                count_rows = 0;
                                            }

                                            _Log_Info_With_Protocol($"Количество обработаных записей {count_rows}");
                                            SQL_transfer_info_upd = GetSqlTextFor.UpdateTransferInfo()
                                                .Replace(":transfer_id", id_transfer).Replace(":TAXFORM", forma)
                                                .Replace(":status_code", "8").Replace(":status_text", "Загружено")
                                                .Replace(":count", count_rows.ToString())
                                                .Replace("status_code=2", "status_code=8")
                                                .Replace("':IFNS'", array_ifns[0]);
                                            SQL_transfer_pack_ins = SQL_transfer_pack_ins
                                                .Replace(":transfer_id", id_transfer).Replace(":TAXFORM", forma)
                                                .Replace(":status_code", "1");
                                            SQL_validation_ins5 = SQL_validation_ins
                                                .Replace(":transfer_id", id_transfer).Replace(":TAXFORM", forma)
                                                .Replace(":step", "5").Replace(":status_code", "success")
                                                .Replace(":status_text", "");
                                            SQL_validation_ins6 = SQL_validation_ins
                                                .Replace(":transfer_id", id_transfer).Replace(":TAXFORM", forma)
                                                .Replace(":step", "6").Replace(":status_code", "success")
                                                .Replace(":status_text", "");
                                            SQL_validation_ins8 = SQL_validation_ins
                                                .Replace(":transfer_id", id_transfer).Replace(":TAXFORM", forma)
                                                .Replace(":step", "8").Replace(":status_code", "success")
                                                .Replace(":status_text", "");
                                            SQL_report_vers_ins = SQL_report_vers_ins
                                                .Replace(":transfer_id", id_transfer).Replace(":TAXFORM", forma)
                                                .Replace(":version_reg", rep_vers).Replace(":login", login_user)
                                                .Replace(":status", "Загружено")
                                                .Replace(":count", count_rows.ToString())
                                                .Replace("':ifns'", list_ifns[0]).Replace("':kpp'", array_kpp[0])
                                                .Replace("':oktmo'", array_oktmo[0]);
                                            //обновление report_vers
                                            var upd_rep_vers = SetparamQuery(GetSqlTextFor.UpdateReportVers()
                                                .Replace(":TAXFORM", forma)
                                                .Replace(":login", login_user)
                                                .Replace(":count", count_rows.ToString())
                                                .Replace(":transfer_id", transfer_id) 
                                                .Replace("':IFNS'", list_ifns[0]));
                                            SQL_update_report_ver = upd_rep_vers;
                                            _Log_Info_With_Protocol($"Обновляем запись первой версиии   {SQL_get_report_ver}");
                                            pg_conn.Close();
                                            pg_conn.Open();
                                            var command_upd_vers = new NpgsqlCommand(SQL_update_report_ver, pg_conn);
                                            command_upd_vers.ExecuteNonQuery();
                                            command_upd_vers.Dispose();
                                            _Log_Info_With_Protocol("Обновление таблиц тех монитора");
                                            var command = new NpgsqlCommand(SQL_transfer_info_upd, pg_mon_conn);
                                            command.ExecuteNonQuery();
                                            //command.CommandText = SQL_transfer_pack_ins;
                                            //command.ExecuteNonQuery();
                                            command.CommandText = SQL_validation_ins5;
                                            command.ExecuteNonQuery();
                                            command.CommandText = SQL_validation_ins6;
                                            command.ExecuteNonQuery();
                                            //command.CommandText = SQL_report_vers_ins;
                                            //command.ExecuteNonQuery();
                                            command.CommandText = SQL_validation_ins8;
                                            command.ExecuteNonQuery();
                                            command.Dispose();
                                            pg_conn.Close();
                                            pg_conn.Open();
                                            _Log_Info_With_Protocol($"Удаление предыдущего среза transfer_id <>  {id_transfer}");
                                            var post_SQL_ifns = post_SQL;
                                            post_SQL_ifns = SetparamQuery(post_SQL_ifns).Replace(":transfer_id", id_transfer).Replace(":TAXFORM", forma);
                                            _Log_Info_With_Protocol(post_SQL_ifns);
                                            var comm_post_sql = new NpgsqlCommand(post_SQL_ifns, pg_conn);
                                            comm_post_sql.ExecuteNonQuery();
                                            pg_conn.Close();
                                            pg_conn.Open();
                                        }
                                    }
                                }
                                catch (Exception e)
                                {
                                    var SQL_transfer_info_upd_error = SQL_transfer_info_upd;
                                    SQL_transfer_info_upd_error = GetSqlTextFor.UpdateTransferInfo()
                                        .Replace(":transfer_id", id_transfer).Replace(":TAXFORM", forma)
                                        .Replace(":status_code", "5").Replace(":status_text", "Ошибка")
                                        .Replace("Загружено", "Ошибка: данные не получены, загрузка не удалась")
                                        .Replace(":count", count_rows.ToString())
                                        .Replace("':IFNS'", array_ifns[0].Trim())
                                        .Replace("status_code=2", "status_code=5");
                                    var command = new NpgsqlCommand(SQL_transfer_info_upd_error, pg_mon_conn);
                                    command.ExecuteNonQuery();
                                    pg_conn.Close();
                                    pg_conn.Open();
                                }
                            }
                        }

                        ;
                    }

                    ora_conn.Close();
                    pg_mon_conn.Close();
                    pg_conn.Close();
                }

                //*************************************************************************************************************
                //Справочники
                //*********************************************************************************************************
                if (flw.type_flow == TYPE_FLOW.HANDBOOK)
                {
                    _Log_Info_With_Protocol($"Обновление справочника {flw.target_table}");
                    var SQL_transfer_info_ins = GetSqlTextFor.InsertToTransferInfo();
                    var SQL_transfer_pack_ins = GetSqlTextFor.InsertToTransferPackInfo();
                    var SQL_transfer_info_upd = GetSqlTextFor.UpdateTransferInfo();
                    var SQL_validation_ins = GetSqlTextFor.InsertToValidationLog();
                    var SQL_validation_ins5 = "";
                    var SQL_validation_ins6 = "";
                    var SQL_COPY_sprv = SQL_COPY_flw;
                    target_table = flw.target_table;
                    var taxform = flw.taxform;
                    var post_SQL = flw.update_target;

                    SQL = flw.source_query_text.ToUpper();
                    SQL = SQL.Replace(":LOGIN", login_user).Replace(":DATE", create_date);
                    transfer_id = GetTransferID(flw.taxpack, flw.name);

                    _Log_Info_With_Protocol($"transfer_id =  {transfer_id}");
                    // var SQL_COPY_sprv = SQL_COPY_flw;
                    SQL = SQL.Replace("TRANSFER_ID", transfer_id);
                    SQL_COPY_sprv = GetSqlTextFor.GetCopyFlowFromStdin(REPLACE_DELIMITER)
                        .Replace("{TABLE_NAME}", target_table);
                    // String SQL_trunc = "truncate table " + target_table;

                    using (var command_ins = new NpgsqlCommand(SQL_transfer_info_ins, pg_mon_conn))
                    {
                        var sql = SetparamQuery(GetSqlTextFor.InsertToTransferInfo());
                        SQL_transfer_info_ins = GetSqlTextFor.InsertToTransferInfo()
                            .Replace(":transfer_id", transfer_id)
                            .Replace(":TAXPACK", "")
                            .Replace(":TAXFORM", taxform)
                            .Replace(":transfer_type", "1")
                            .Replace(":status_code", "1")
                            .Replace(":stage_table", target_table)
                            .Replace(":status_text", "В процессе загрузки")
                            .Replace(":TAXPAYER", "")
                            .Replace(":IFNS", "")
                            .Replace(":KPP", "")
                            .Replace(":OKTMO", "")
                            .Replace(":FISCAL_YEAR", "0")
                            .Replace(":PERIOD", "")
                            .Replace(":CORR_NUM", "0");

                        command_ins.CommandText = SQL_transfer_info_ins;
                        pg_mon_conn.Open();
                        var dataReader = command_ins.ExecuteReader();
                        command_ins.Dispose();
                        pg_mon_conn.Close();
                    }

                    // _Log_Info_With_Protocol("Очистка таблицы справочника");
                    // using (NpgsqlCommand command = new NpgsqlCommand(SQL_trunc, pg_conn))
                    // {
                    //     pg_conn.Open();
                    //     command.ExecuteNonQuery();
                    //     pg_conn.Close();
                    // }

                    _Log_Info_With_Protocol("Чтение таблицы справочника");
                    ora_conn.Open();
                    var com_sql = new OracleCommand(SQL, ora_conn);
                    using (var oracleReader = com_sql.ExecuteReader())
                    {
                        var filed_count = oracleReader.FieldCount;
                        pg_conn.Open();
                        var count_rows = 0;
                        using (var writer = pg_conn.BeginTextImport(SQL_COPY_sprv))
                        {
                            while (oracleReader.Read())
                            {
                                var row = string.Join(GetSqlTextFor.Delimiter, GetProcessedColumnValuesFromOracleToPg(oracleReader))
                                        .Replace("transfer_id", transfer_id)
                                        .Replace("\r\n", " ")
                                        .Replace("\n", " ")
                                        .Replace("\r", " ")
                                    ;
                                //row = row.Replace("version_reg", rep_vers);
                                writer.WriteLine(row);
                                count_rows++;
                            }
                        }

                        SQL_transfer_info_upd = SQL_transfer_info_upd.Replace(":count", count_rows.ToString());
                        SQL_transfer_pack_ins = SQL_transfer_pack_ins.Replace(":transfer_id", transfer_id)
                            .Replace(":TAXFORM", flw.taxform)
                            .Replace(":status_code", "1");
                    }

                    ora_conn.Close();
                    pg_conn.Close();

                    _Log_Info_With_Protocol("Обновление таблиц тех монитора");
                    using (var command = new NpgsqlCommand(SQL_transfer_info_upd, pg_mon_conn))
                    {
                        var sql = SetparamQuery(SQL_transfer_info_upd);
                        SQL_transfer_info_upd = sql.Replace(":transfer_id", transfer_id)
                            .Replace(":TAXPACK", Tax_pack)
                            .Replace(":TAXFORM", taxform)
                            .Replace(":transfer_type", "1")
                            .Replace("status_code=2", "status_code=8")
                            .Replace(":stage_table", target_table)
                            .Replace(":status_text", "Загружено");
                        SQL_transfer_info_upd = SQL_transfer_info_upd.Replace(":TAXPAYER", "-").Replace(":IFNS", "-")
                            .Replace(":KPP", "-").Replace(":OKTMO", "-").Replace(":FISCAL_YEAR", "0000")
                            .Replace(":PERIOD", "-").Replace(":CORR_NUM", "0");
                        command.CommandText = SQL_transfer_info_upd;
                        pg_mon_conn.Open();
                        var dataReader = command.ExecuteReader();
                        dataReader.Close();
                        command.CommandText = SQL_transfer_pack_ins;
                        dataReader = command.ExecuteReader();
                        dataReader.Close();

                        SQL_validation_ins5 = SQL_validation_ins.Replace(":transfer_id", transfer_id)
                            .Replace(":TAXFORM", flw.taxform).Replace(":step", "5").Replace(":status_code", "success")
                            .Replace(":status_text", "");
                        SQL_validation_ins6 = SQL_validation_ins.Replace(":transfer_id", transfer_id)
                            .Replace(":TAXFORM", flw.taxform).Replace(":step", "6").Replace(":status_code", "success")
                            .Replace(":status_text", "");
                        command.CommandText = SQL_validation_ins5;
                        dataReader = command.ExecuteReader();
                        dataReader.Close();
                        command.CommandText = SQL_validation_ins6;
                        dataReader = command.ExecuteReader();
                        dataReader.Close();
                        command.Dispose();
                        pg_mon_conn.Close();
                    }
                }

                //*************************************************************************************************************
                // Книги, журналы, VAT_transaction
                //***************************************************************************************
                if ((flw.type_flow == TYPE_FLOW.BOOKS_JOURNALS || flw.type_flow == TYPE_FLOW.BOOKS_VAT) &&
                    flw.taxpack == TAXPACK.D02)
                {
                    var ifns_code = "";
                    var kpp_code = "";
                    var oktm_code = "";
                    var SQL_transfer_info_ins = GetSqlTextFor.InsertToTransferInfo();
                    var SQL_transfer_pack_ins = GetSqlTextFor.InsertToTransferPackInfo();
                    var SQL_transfer_info_upd = GetSqlTextFor.UpdateTransferInfo();
                    var SQL_validation_ins = GetSqlTextFor.InsertToValidationLog();
                    var SQL_validation_ins5 = "";
                    var SQL_validation_ins6 = "";
                    _Log_Info_With_Protocol("Передача  Книги, журналы, VAT_transaction");
                    var filial_new = 0;
                    var sql_delta = flw.calculation_delta_val;
                    var post_SQL = flw.update_target;
                    var get_list_forms = flw.taxform; //список филиалов
                    target_table = flw.target_table; // целевая таблица
                    _Log_Info_With_Protocol(target_table);
                    var SQL_COPY =  GetSqlTextFor.GetCopyFlowFromStdinWithNullMarker(REPLACE_DELIMITER); // var SQL_COPY = SQL_COPY_flw;
                    SQL_COPY = SQL_COPY.Replace("{TABLE_NAME}", target_table); //запрос на вставку
                    get_list_forms = SetparamQuery(get_list_forms); // замена параметров на значения
                    post_SQL = SetparamQuery(post_SQL);
                    pg_conn.Open();
                    //определяем последню дату загрузки
                    var last_date_sql = SetparamQuery(flw.calculation_delta_val); //формируем запрос
                    var command_delta_val = new NpgsqlCommand(last_date_sql, pg_conn);
                    var dataReader_dela = command_delta_val.ExecuteScalar();
                    var last_date = dataReader_dela.ToString();
                    _Log_Info_With_Protocol($"Последняя дата загрузки: {last_date}");
                    pg_conn.Close();

                    get_list_forms = SetparamQuery(get_list_forms)
                            .Replace(":date", last_date); //запрос на выборку филиалов после последней загрузки
                    string[] array_filials = null;

                    com = new OracleCommand(get_list_forms, ora_conn);
                    ora_conn.Open();
                    // получаем перечень филиалов
                    array_filials = null;
                    _Log_Info_With_Protocol("Получаем перечень филиалов");
                    using (var oracleReader = com.ExecuteReader())
                    {
                        var value = "";
                        while (oracleReader.Read())
                            if (oracleReader.IsDBNull(0) | (oracleReader.GetValue(0).ToString() == "-"))
                            {
                                _Log_Info_With_Protocol("Новые записи по филиалам не найдены");
                            }
                            else
                            {
                                filial_new = 1;
                                value = (string)oracleReader.GetOracleString(0);
                                ifns_code = oracleReader.GetOracleString(1).ToString().Replace("'", "");
                                kpp_code = oracleReader.GetOracleString(2).ToString().Replace("'", "");
                                oktm_code = oracleReader.GetOracleString(3).ToString().Replace("'", "");
                                array_filials = value.Split(';');
                            }
                    }

                    ora_conn.Close();
                    if (filial_new == 1)
                    {
                        //формируем transfer_id для филиалов
                        transfer_id = GetTransferID(flw.taxpack, flw.name);
                        _Log_Info_With_Protocol($"transfer_id =  {transfer_id}");

                        using (var command_ins = new NpgsqlCommand(SQL_transfer_info_ins, pg_mon_conn))
                        {
                            var sql = SetparamQuery(GetSqlTextFor.InsertToTransferInfo());
                            if (flw.type_flow == "VAT")
                            {
                                SQL_transfer_info_ins = GetSqlTextFor.InsertToTransferInfo()
                                    .Replace(":transfer_id", transfer_id)
                                    .Replace(":TAXPACK", "")
                                    .Replace(":TAXFORM", flw.name)
                                    .Replace(":transfer_type", "3")
                                    .Replace(":status_code", "1")
                                    .Replace(":stage_table", target_table)
                                    .Replace(":status_text", "В процессе загрузки")
                                    .Replace(":TAXPAYER", "")
                                    .Replace(":IFNS", "")
                                    .Replace(":KPP", "")
                                    .Replace(":OKTMO", "")
                                    .Replace(":FISCAL_YEAR", "null")
                                    .Replace(":PERIOD", "")
                                    .Replace(":CORR_NUM", "0")
                                    .Replace(":login", login_user);
                                SQL_transfer_info_ins = SetparamQuery(SQL_transfer_info_ins);
                            }
                            else
                            {
                                SQL_transfer_info_ins = GetSqlTextFor
                                    .InsertToTransferInfo()
                                    .Replace(":transfer_id", transfer_id)
                                    .Replace(":TAXPACK", flw.taxpack)
                                    .Replace(":TAXFORM", flw.name)
                                    .Replace(":transfer_type", "2")
                                    .Replace(":status_code", "1")
                                    .Replace(":IFNS", ifns_code)
                                    .Replace(":stage_table", target_table);
                                    
                                SQL_transfer_info_ins = SetparamQuery(SQL_transfer_info_ins)
                                    .Replace(":status_text", "В процессе загрузки")
                                    .Replace(":TAXPAYER", ":TAXPAYER")
                                    .Replace(":login", login_user)
                                    ;
                            }
                            
                            command_ins.CommandText = SQL_transfer_info_ins;
                            pg_mon_conn.Open();
                            command_ins.ExecuteNonQuery();
                            command_ins.Dispose();
                            pg_mon_conn.Close();
                        }
                        if (flw.type_flow != "VAT")
                        {
                          var SQL_get_report_ver = SetparamQuery(GetSqlTextFor.SelectReportVers())
                              .Replace("' '", "''")
                              .Replace("''", "'");
                        SQL_get_report_ver = SQL_get_report_ver
                            .Replace(":TAXFORM", flw.name)
                            .Replace(":ifns", ifns_code)
                            .Replace(":kpp", kpp_code)
                            .Replace(":oktmo", oktm_code);
                        SQL_get_report_ver = SetparamQuery(SQL_get_report_ver)
                            .Replace("' '", "''");
                        pg_conn.Open();
                        var command_get_vers = new NpgsqlCommand(SQL_get_report_ver, pg_conn);
                        if (flw.type_flow == TYPE_FLOW.BOOKS_JOURNALS)
                        {
                            var dataReader_vers = command_get_vers.ExecuteScalar();
                            rep_vers = dataReader_vers.ToString();
                            if (rep_vers == "1")
                            {
                                //если определена версия 1, то добовляем новую запись
                                var SQL_report_vers_insert = "";
                                _Log_Info_With_Protocol($"запись первой версиии    {0}, {SQL_get_report_ver}");
                                SQL_report_vers_insert = GetSqlTextFor.InsertToReportVers();
                                SQL_report_vers_insert = SQL_report_vers_insert.Replace(":TAXFORM", flw.name)
                                    .Replace(":version_reg", "1").Replace(":status", "").Replace(":count", "0")
                                    .Replace(":transfer_id", transfer_id).Replace(":ifns", ifns_code)
                                    .Replace(":kpp", kpp_code).Replace(":oktmo", oktm_code)
                                    .Replace(":login", login_user);
                                SQL_report_vers_insert = SetparamQuery(SQL_report_vers_insert).Replace("' '", "''");
                                var command_ins_vers = new NpgsqlCommand(SQL_report_vers_insert, pg_conn);
                                var dataReader_ins_vers = command_ins_vers.ExecuteNonQuery();
                                command_ins_vers.Dispose();
                                SQL_report_vers_insert = GetSqlTextFor.InsertToReportVers();
                            }
                            else
                            {
                                _Log_Info_With_Protocol("Запись первой версиии для ИФНС  существует ");
                            }
                        } 
                        }
    

                        pg_conn.Close();
                        var all_record = 0; //Общее количество записей по филиалам
                        foreach (var filial in array_filials)
                        {
                            _Log_Info_With_Protocol("Филиал  " + filial);
                            //запрос данных
                            SQL = flw.source_query_text.Replace(":transfer_id", transfer_id)
                                .Replace(":login", login_user).Replace(":date", create_date);
                            foreach (var filtr in parameters)
                            {
                                var key = filtr.Key;
                                var param = ":" + filtr.Key;
                                var val = filtr.Value;
                                SQL = SQL.Replace(param, val);
                            }

                            SQL = SQL.Replace(":FILIAL", filial).Replace(":login", login_user);
                            if (ora_conn.State == ConnectionState.Closed)
                                ora_conn.Open();
                            com = new OracleCommand(SQL, ora_conn);
                            //чтение данных

                            using (var oracleReader = com.ExecuteReader())
                            {
                                var filed_count = oracleReader.FieldCount;
                                pg_conn.Open();
                                var count_rec = 0;
                                using (var writer = pg_conn.BeginTextImport(SQL_COPY))
                                {
                                    try
                                    {
                                        while (oracleReader.Read())
                                        {
                                            var row = string.Join(GetSqlTextFor.Delimiter, GetProcessedColumnValuesFromOracleToPg(oracleReader))
                                                .Replace("\r\n", " ").Replace("\n", " ").Replace("\r", " ")
                                                .Replace(":version_reg", "1").Replace(":transfer_id", transfer_id)
                                                .Replace("\\", "/").ToString();
                                            
                                            writer.WriteLine(row);
                                            count_rec++;
                                        }
                                       
                                    }
                                    catch (Exception ex)
                                    {
                                        status_etl = "Новые данные по филиалу не найдены " + filial +  " " + ex.Demystify();
                                        var exc = new UnexpectedImportErrorException(status_etl);
                                        _Log_Error_to_Protocol(exc, status_etl);

                                    }
                                    writer.Close(); 
                                }

                                all_record = all_record + count_rec;
                                pg_conn.Close();
                            }
                        }

                        //заполняем статус загрузки филиалов
                        using (var command = new NpgsqlCommand(SQL_transfer_info_upd, pg_mon_conn))
                        {
                            var sql = SetparamQuery(SQL_transfer_info_upd);
                            if (flw.type_flow != "VAT")
                            {
                                sql = sql.Replace(":transfer_id", transfer_id)
                                    .Replace(":TAXPACK", Tax_pack)
                                    .Replace(":TAXFORM", flw.name)
                                    .Replace(":count", all_record.ToString())
                                    .Replace(":transfer_type", "2")
                                    .Replace("status_code=2", "status_code=8")
                                    .Replace(":stage_table", target_table)
                                    .Replace(":status_text", "Загружено");
                                sql = sql.Replace(":TAXPAYER", "-").Replace(":IFNS", "-").Replace(":KPP", "-")
                                    .Replace(":OKTMO", "-").Replace(":FISCAL_YEAR", "0000").Replace(":PERIOD", "-")
                                    .Replace(":CORR_NUM", "0");
                                command.CommandText = sql.Replace("AND IFNS =' '", "");
                                pg_mon_conn.Open();
                                command.ExecuteNonQuery();
                                SQL_transfer_pack_ins = SQL_transfer_pack_ins.Replace(":transfer_id", transfer_id)
                                    .Replace(":TAXPACK", Tax_pack)
                                    .Replace(":TAXFORM", flw.name)
                                    .Replace(":status_code", "1");
                                command.CommandText = SQL_transfer_pack_ins;
                                command.ExecuteNonQuery(); 
                            }
                            else
                            {
                                sql = sql.Replace(":transfer_id", transfer_id)
                                    .Replace(":TAXPACK", "")
                                    .Replace(":TAXFORM", flw.name)
                                    .Replace(":count", all_record.ToString())
                                    .Replace(":transfer_type", "3")
                                    .Replace("status_code=2", "status_code=2")
                                    .Replace(":stage_table", target_table)
                                    .Replace(":status_text", "Загружено");
                                sql = sql.Replace(":TAXPAYER", "").Replace(":IFNS", "").Replace(":KPP", "")
                                    .Replace(":OKTMO", "").Replace(":FISCAL_YEAR", "0000").Replace(":PERIOD", "-")
                                    .Replace(":CORR_NUM", "0");
                                command.CommandText = sql.Replace("AND IFNS =' '", "");
                                pg_mon_conn.Open();
                                command.ExecuteNonQuery();
                                SQL_transfer_pack_ins = SQL_transfer_pack_ins.Replace(":transfer_id", transfer_id)
                                    .Replace(":TAXPACK", "")
                                    .Replace(":TAXFORM", flw.name)
                                    .Replace(":status_code", "1");
                                command.CommandText = SQL_transfer_pack_ins;
                                command.ExecuteNonQuery(); 
                                
                            }

                            SQL_validation_ins5 = SQL_validation_ins.Replace(":transfer_id", transfer_id)
                                .Replace(":TAXFORM", flw.name).Replace(":step", "5").Replace(":status_code", "success")
                                .Replace(":status_text", "");
                            SQL_validation_ins6 = SQL_validation_ins.Replace(":transfer_id", transfer_id)
                                .Replace(":TAXFORM", flw.name).Replace(":step", "6").Replace(":status_code", "success")
                                .Replace(":status_text", "");
                            command.CommandText = SQL_validation_ins5;
                            command.ExecuteNonQuery();
                            command.CommandText = SQL_validation_ins6;
                            command.ExecuteNonQuery();
                            command.Dispose();
                            pg_mon_conn.Close();
                        }

                        if (flw.type_flow == TYPE_FLOW.BOOKS_JOURNALS)
                        {
                            pg_conn.Open();
                            var SQL_report_vers_upd = "";
                            SQL_report_vers_upd = SetparamQuery(GetSqlTextFor.UpdateReportVers()
                                .Replace(":transfer_id", transfer_id)
                                .Replace(":TAXFORM", flw.name)
                                .Replace(":login", login_user)
                                .Replace(":IFNS", ifns_code)
                                .Replace(":count", all_record.ToString()));
                            var command = new NpgsqlCommand(SQL_report_vers_upd, pg_conn);
                            command.ExecuteNonQuery();
                            pg_conn.Close();
                        }

                        ora_conn.Close();

                        _Log_Info_With_Protocol($"Удаление предыдущего среза transfer_id <>  {transfer_id}");
                        var pg_conn_upd = new NpgsqlConnection(stage_conn);
                        post_SQL = post_SQL.Replace(":transfer_id", transfer_id);
                        _Log_Info_With_Protocol(post_SQL);
                        pg_conn_upd.Open();
                        var command_update_sql = new NpgsqlCommand(post_SQL, pg_conn_upd);
                        var dataReader_upd = command_update_sql.ExecuteNonQuery();
                        pg_conn_upd.Close();
                        _Log_Info_With_Protocol("Обновление таблиц тех монитора");
                    }
                }

                //*************************************************************************************************************
                // транзакционные данные
                //***************************************************************************************
                if (flw.type_flow == TYPE_FLOW.TRANSACTIONAL_DATA)
                {
                    var status_td = LoadTransactionData(flw, ora_conn, pg_conn, pg_mon_conn, create_date, login_user,
                        PACKAGE_SIZE);
                }

                _Log_Info_With_Protocol($"Поток передачи {flw.target_table} загружен");
            }

            _Log_Info_With_Protocol("Обработка потоков завершена");
            status_etl = "данные переданы";
        }
        catch (Exception ex)
        {
            if (new_data)
            {
                status_etl = "Новые данные не найдены. " + ex.Demystify();

                var exc = new UnexpectedImportErrorException(status_etl);
                _Log_Error_to_Protocol(exc, status_etl);
                throw exc;
            }

            status_etl = "Ошибка передачи данных: " + ex.Message;
            // Log.Error(ex.Message);
            _Log_Error_to_Protocol(ex, ex.Message);
            throw new UnexpectedImportErrorException(status_etl);
        }

        // Log.CloseAndFlush();
        ResultOperations["status"] = status_etl;
        ResultOperations["protocol"] = LogProtocol.ToString();

        return ResultOperations; // return status_etl;
    }

    private int LoadTransactionData(flow flw, OracleConnection ora_conn, NpgsqlConnection pg_conn, NpgsqlConnection pg_mon_conn, string create_date, string login_user, int PackageSize)
    {
        TextWriter writer_import = null;
        var status = 0;
        var flag_tech_mon = 0;
        var transfer_id = GetTransferID(flw.taxpack, flw.taxform);
        var target_table = flw.target_table;
        var SQL_delta = flw.calculation_delta_val;
        // var SQL_COPY_TD = SQL_COPY_flw;
        var SQL_COPY_TD = GetSqlTextFor.GetCopyFlowFromStdin(REPLACE_DELIMITER);
        var SQL = flw.source_query_text.ToUpper();
        var SQL_transfer_info_ins = GetSqlTextFor.InsertToTransferInfo();
        var SQL_transfer_pack_ins = GetSqlTextFor.InsertToTransferPackInfo();
        var SQL_transfer_info_upd = GetSqlTextFor.UpdateTransferInfo();
        var SQL_validation_ins = GetSqlTextFor.InsertToValidationLog();
        var SQL_validation_ins5 = "";
        var SQL_validation_ins6 = "";
        var log_id = "";
        var sql_del = flw.update_target;
        //string sql_del = "delete from {table} WHERE log_id=:log_id";
        _Log_Info_With_Protocol($"Передача транзакционных данных {flw.target_table}");
        SQL_COPY_TD = SQL_COPY_TD.Replace("{TABLE_NAME}", target_table); //запрос на вставку
        SQL_transfer_info_ins = SQL_transfer_info_ins.Replace(":FISCAL_YEAR", "null").Replace(":CORR_NUM", "0")
            .Replace(":TAXPACK", "");
        SQL_transfer_info_ins = SetparamQuery(SQL_transfer_info_ins).Replace("' '", "''").Replace("'TD '", "''");
        SQL_validation_ins = SetparamQuery(SQL_validation_ins).Replace("' '", "''");
        SQL_transfer_pack_ins = SetparamQuery(SQL_transfer_pack_ins).Replace("' '", "''");
        // получаем последний LOG_ID загруженных данных                  
        try
        {
            var status_connect = pg_conn.State & ConnectionState.Open;
            var status_connect_mon = pg_conn.State & ConnectionState.Open;
            if (status_connect == ConnectionState.Closed)
            {
                pg_conn.Open();
                ora_conn.Open();
                //pg_mon_conn.Open();
            }

            if (status_connect_mon == ConnectionState.Closed) pg_mon_conn.Open();

            var sql = "";
            try
            {
                var pg_command = new NpgsqlCommand(SQL_delta, pg_conn);
                log_id = pg_command.ExecuteScalar().ToString();
                _Log_Info_With_Protocol($"Log_id {log_id}");
                sql = SetparamQuery(SQL_transfer_info_ins);
                if (log_id.IndexOf("between") > 0)
                {
                    var log_id_between = log_id.Split(';');
                    SQL = SQL.Replace(":LOG_ID_MIN", log_id_between[1]).Replace(":LOG_ID_MAX", log_id_between[2])
                        .Replace(":TRANSFER_ID", transfer_id).Replace(":TAXFORM", flw.taxform)
                        .Replace(":LOGIN", login_user).Replace(":DATE", create_date);
                    sql_del = sql_del.Replace("{table}", flw.target_table).Replace(":log_id", log_id_between[1]);
                }
                else
                {
                    SQL = SQL.Replace(":TRANSFER_ID", transfer_id).Replace(":LOG_ID", log_id)
                        .Replace(":TAXFORM", flw.taxform).Replace(":LOGIN", login_user).Replace(":DATE", create_date);
                    sql_del = sql_del.Replace("{table}", flw.target_table).Replace(":log_id", log_id);
                }

                if (flw.name.IndexOf("NSI") > 0)
                {
                    SQL_transfer_info_ins = sql.Replace(":transfer_id", transfer_id)
                        .Replace(":TAXFORM", flw.taxform)
                        .Replace(":transfer_type", "1")
                        .Replace(":status_code", "1")
                        .Replace(":stage_table", target_table)
                        .Replace(":status_text", "В процессе загрузки")
                        .Replace(":TAXPACK", "")
                        .Replace(":login", login_user);
                }
                else
                {
                    SQL_transfer_info_ins = sql.Replace(":transfer_id", transfer_id)
                        .Replace(":TAXFORM", flw.taxform)
                        .Replace(":transfer_type", "3")
                        .Replace(":status_code", "1")
                        .Replace(":stage_table", target_table)
                        .Replace(":status_text", "В процессе загрузки")
                        .Replace(":TAXPACK", "")
                        .Replace(":login", login_user); 
                }
                
                var command_transfer_ins = new NpgsqlCommand(SQL_transfer_info_ins, pg_mon_conn);
                command_transfer_ins.ExecuteNonQuery();
                command_transfer_ins.Dispose();

                sql = SetparamQuery(GetSqlTextFor.InsertToTransferPackInfo());
                SQL_transfer_pack_ins = sql.Replace(":transfer_id", transfer_id)
                    .Replace(":TAXFORM", flw.taxform)
                    .Replace(":transfer_type", "3")
                    .Replace(":status_code", "1")
                    .Replace(":stage_table", target_table)
                    .Replace(":status_text", "В процессе загрузки")
                    .Replace(":TAXPACK", "")
                    .Replace(":login", login_user);;
                try
                {
                    //NpgsqlCommand command_transfer_ins = new NpgsqlCommand(SQL_transfer_info_ins, pg_mon_conn);
                    //command_transfer_ins.ExecuteNonQuery();
                    //--command_ins.CommandText = SQL_transfer_pack_ins;
                    //--command_ins.ExecuteNonQuery();
                    //command_transfer_ins.Dispose();

                    // удаление последнего загруженного Log_id
                    try
                    {
                        var command_del_log_id = new NpgsqlCommand(sql_del, pg_conn);
                        command_del_log_id.ExecuteNonQuery();
                        command_del_log_id.Dispose();
                        _Log_Info_With_Protocol(SQL);
                        var ora_read = new OracleCommand(SQL, ora_conn);
                        try
                        {
                            using (var oracleReader = ora_read.ExecuteReader())
                            {
                                var filed_count = oracleReader.FieldCount;
                                var new_package = true;
                                var count_rows = 0;
                                var rownum = 0;
                                // writer_import = pg_conn.BeginTextImport(SQL_COPY_TD); 
                                while (oracleReader.Read()) {
                                    if (oracleReader.IsDBNull(0))
                                    {
                                        _Log_Info_With_Protocol("Новые записи не найдены");
                                        SQL_transfer_info_upd = SQL_transfer_info_upd
                                            .Replace(":transfer_id", transfer_id).Replace(":TAXFORM", flw.taxform)
                                            .Replace("status_code=2", "status_code=8")
                                            .Replace(":status_text", "Загружено").Replace(":count", "0");
                                        SQL_transfer_pack_ins = SQL_transfer_pack_ins
                                            .Replace(":transfer_id", transfer_id).Replace(":TAXFORM", flw.taxform)
                                            .Replace(":status_code", "1");
                                        try
                                        {
                                            pg_mon_conn.Open();
                                            var command = new NpgsqlCommand(SQL_transfer_info_upd, pg_mon_conn);
                                            command.ExecuteNonQuery();
                                            command.CommandText = SQL_transfer_pack_ins;
                                            command.ExecuteNonQuery();
                                            command.CommandText = SQL_validation_ins5;
                                            command.ExecuteNonQuery();
                                            command.CommandText = SQL_validation_ins6;
                                            command.ExecuteNonQuery();
                                            command.Dispose();
                                            pg_mon_conn.Close();
                                            pg_conn.Close();
                                            ora_conn.Close();
                                            status = 0;
                                        }
                                        catch (Exception ex)
                                        {
                                            // Log.Error("Ошибка при заполнении таблиц тех монитора " + ex.Message);
                                            _Log_Error_to_Protocol(ex, "Ошибка при заполнении таблиц тех монитора " + ex.Message);
                                            status = -1;
                                        }
                                    }
                                    else
                                    {
                                        // if (flag_tech_mon == 0)
                                            // NpgsqlCommand command_ins = new NpgsqlCommand(SQL_transfer_info_ins, pg_mon_conn);
                                            //  command_ins.ExecuteNonQuery();
                                            //  command_ins.Dispose();
                                            //  pg_mon_conn.Close();
                                            // flag_tech_mon = 1;

                                        if (new_package)
                                        {
                                            writer_import = pg_conn.BeginTextImport(SQL_COPY_TD);
                                        }
                                        
                                        var row = string.Join(GetSqlTextFor.Delimiter, GetProcessedColumnValuesFromOracleToPg(oracleReader))
                                            .Replace("\r\n", " ").Replace("\n", " ").Replace("\r", " ")
                                            .Replace(":version_reg", "1").Replace(":transfer_id", transfer_id)
                                            .Replace("\\", "/");
                                        
                                        writer_import.WriteLine(row);
                                        count_rows++;
                                        rownum++;
                                        new_package = false;
                                        if (rownum == PackageSize)
                                        {
                                            _Log_Info_With_Protocol("Фиксация пакета");
                                            writer_import.Close();
                                            new_package = true;
                                            rownum = 0;
                                        }
                                    }
                                }
                                
                                if (count_rows > 0) writer_import.Close();

                                _Log_Info_With_Protocol("Обновление таблиц тех монитора");
                                if (flw.name.IndexOf("NSI") > 0)
                                    SQL_transfer_info_upd = SQL_transfer_info_upd.Replace(":transfer_id", transfer_id)
                                        .Replace(":TAXFORM", flw.taxform).Replace("status_code=2", "status_code=8")
                                        .Replace(":status_text", "Загружено").Replace(":count", count_rows.ToString())
                                        .Replace("AND IFNS =':IFNS'", "");
                                else
                                    SQL_transfer_info_upd = SQL_transfer_info_upd.Replace(":transfer_id", transfer_id)
                                        .Replace(":TAXFORM", flw.taxform).Replace(":status_text", "Загружено")
                                        .Replace(":count", count_rows.ToString()).Replace("AND IFNS =':IFNS'", "");

                                SQL_transfer_pack_ins = SQL_transfer_pack_ins.Replace(":transfer_id", transfer_id)
                                    .Replace(":TAXFORM", flw.taxform).Replace(":status_code", "1");
                                SQL_validation_ins5 = SQL_validation_ins.Replace(":transfer_id", transfer_id)
                                    .Replace(":TAXFORM", flw.taxform).Replace(":step", "5")
                                    .Replace(":status_code", "success").Replace(":status_text", "");
                                SQL_validation_ins6 = SQL_validation_ins.Replace(":transfer_id", transfer_id)
                                    .Replace(":TAXFORM", flw.taxform).Replace(":step", "6")
                                    .Replace(":status_code", "success").Replace(":status_text", "");
                                try
                                {
                                    //pg_mon_conn.Open();
                                    var command = new NpgsqlCommand(SQL_transfer_info_upd, pg_mon_conn);
                                    command.ExecuteNonQuery();
                                    command.CommandText = SQL_transfer_pack_ins;
                                    command.ExecuteNonQuery();
                                    command.CommandText = SQL_validation_ins5;
                                    command.ExecuteNonQuery();
                                    command.CommandText = SQL_validation_ins6;
                                    command.ExecuteNonQuery();
                                    command.Dispose();
                                    status = 0;
                                }
                                catch (Exception ex)
                                {
                                    // Log.Error($"Ошибка при заполнении таблиц тех монитора {ex.Message}");
                                    _Log_Error_to_Protocol(ex,
                                        $"Ошибка при заполнении таблиц тех монитора {ex.Message}");
                                    SetErrorStatusTransferInfo(transfer_id, pg_mon_conn);
                                    status = -1;
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            //Log.Error($"Ошибка при передаче данных {ex.Message}");
                            _Log_Error_to_Protocol(ex, $"Ошибка при передаче данных {ex.Message}");
                            SetErrorStatusTransferInfo(transfer_id, pg_mon_conn);
                            status = -1;
                        }
                    }
                    catch (Exception ex)
                    {
                        // Log.Error($"Ошибка при удалении последнего загруженого log_id {log_id}, {ex.Message}");
                        _Log_Error_to_Protocol(ex,
                            $"Ошибка при удалении последнего загруженого log_id {log_id}, {ex.Message}");
                        SetErrorStatusTransferInfo(transfer_id, pg_mon_conn);
                        status = -1;
                    }
                }
                catch (Exception ex)
                {
                    //Log.Error($"Ошибка при заполнении таблиц тех монитора {ex.Message}");
                    _Log_Error_to_Protocol(ex, $"Ошибка при заполнении таблиц тех монитора {ex.Message}");
                    SetErrorStatusTransferInfo(transfer_id, pg_mon_conn);
                    status = -1;
                }

                pg_mon_conn.Close();
                pg_conn.Close();
                ora_conn.Close();
            }
            catch (Exception ex)
            {
                // Log.Error($"Ошибка при запросе последнего загруженного log_id {ex.Message}");
                _Log_Error_to_Protocol(ex, $"Ошибка при запросе последнего загруженного log_id {ex.Message}");
                SetErrorStatusTransferInfo(transfer_id, pg_mon_conn);
                status = -1;
            }
        }
        catch (Exception ex)
        {
            //Log.Error($"Ошибка при открытии подключений к БД {ex.Message}");
            _Log_Error_to_Protocol(ex, $"Ошибка при открытии подключений к БД {ex.Message}");
            status = -1;
        }

        return status;
    }

    public override Dictionary<string, ModuleR12Statuses> import(OnlineStatus status)
    {
        throw new NotImplementedException();
    }

    public static class TYPE_FLOW
    {
        public static readonly string HANDBOOK = "1"; //taxpack = "SP"

        public static readonly string REPORTING_DATA = "2";

        public static readonly string TRANSACTIONAL_DATA = "3"; //taxpack = "TD"

        public static readonly string BOOKS_JOURNALS = "OTH"; // //Книги, журналы, VAT_transaction
        public static readonly string BOOKS_VAT = "VAT"; // VAT_transaction
    }

    public static class TAXPACK
    {
        public static readonly string TRANSACTIONAL = "TD"; // транзакционные данные
        public static readonly string HANDBOOK = "SP"; // справочники

        public static readonly string D02 = "D02"; // //Книги, журналы, VAT_transaction
        // flw.type_flow == ) && (flw.taxpack == "D02"
    }

    public struct flow
    {
        public string name { get; set; }
        public string module { get; set; }
        public string type_flow { get; set; }
        public string taxpack { get; set; }
        public string taxform { get; set; }
        public string queue_connection { get; set; }
        public string queue_table { get; set; }
        public string source_connection { get; set; }
        public string target_connection { get; set; }
        public string calculation_delta_val { get; set; }
        public string source_query_text { get; set; }
        public string target_table { get; set; }
        public string update_target { get; set; }
    }

    private struct service
    {
        public string name { get; set; }
        public string path { get; set; }
        public string port { get; set; }
        public string auth_token_header { get; set; }
        public string auth_token { get; set; }
    }

    private struct connection
    {
        public string id { get; set; }
        public string type { get; set; }
        public string driverClassname { get; set; }
        public string location { get; set; }
        public string port { get; }
        public string host { get; }
        public string service_name { get; }
        public string auth_token { get; set; }
    }


    private struct grpc
    {
        public string id { get; set; }
        public string type { get; }
        public string location { get; }
        public string auth_token { get; set; }
        public string check_method { get; set; }
    }
    
    private static List<string> GetProcessedColumnValuesFromOracleToPg(OracleDataReader oracleReader)
    {
        List<string> columnValueList = new List<string>();

        for (var i = 0; i < oracleReader.FieldCount; i++)
        {
            var value = "";

            if (oracleReader.IsDBNull(i) && (new[] { "Decimal", "Number", "Date" }.Contains(oracleReader.GetDataTypeName(i))))
            {
                columnValueList.Add(NULL_MARKER_FOR_COPY_FROM);
                continue;
            }

            if (!oracleReader.IsDBNull(i))
            {
                switch (oracleReader.GetDataTypeName(i))
                {
                    case "Number":
                    case "Decimal":
                        value = oracleReader.GetString(i).Replace(",", ".");
                        break;
                    case "Date":
                        value = oracleReader.GetDateTime(i).ToString("MM.dd.yyyy");
                        break;
                    default:
                        value = oracleReader.GetString(i).Replace(GetSqlTextFor.Delimiter, "");
                        break;
                }
            }

            columnValueList.Add(value);
        }

        return columnValueList;
    }

}
