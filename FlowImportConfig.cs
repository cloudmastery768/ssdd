using System;
using System.ComponentModel;
using System.Xml.Serialization;

namespace FlowCommon.config
{
    /// <summary>
    ///     Основнйо serialize-class конфига импорта данных из R12_Oracle
    /// </summary>
    [Serializable]
    [DesignerCategory("code")]
    [XmlTypeAttribute(AnonymousType = true)]
    [XmlRootAttribute(Namespace = "", IsNullable = false)]
    public class vnm_r12oracle_import_service
    {
        private vnm_r12oracle_import_servicePreferences preferencesField;
        
        private vnm_r12oracle_import_serviceService serviceField;

        private vnm_r12oracle_import_serviceConnection[] connectionsField;
        
        private vnm_r12oracle_import_serviceFlow[] flowsField;
        
        /// <remarks />
        public vnm_r12oracle_import_servicePreferences preferences
        {
            get => preferencesField;
            set => preferencesField = value;
        }
        /// <remarks />
        public vnm_r12oracle_import_serviceService service
        {
            get => serviceField;
            set => serviceField = value;
        }

        /// <summary>
        ///     Конфигурация подключений
        /// </summary>
        [XmlArrayItemAttribute("connection", IsNullable = true)]
        public vnm_r12oracle_import_serviceConnection[] connections
        {
            get => connectionsField;
            set => connectionsField = value;
        }

        /// <summary>
        ///     Конфигурация потоков
        /// </summary>
        [XmlArrayItemAttribute("flow")]
        public vnm_r12oracle_import_serviceFlow[] flows
        {
            get => flowsField;
            set => flowsField = value;
        }
    }
    
    /// <remarks />
    [Serializable]
    [DesignerCategory("code")]
    [XmlTypeAttribute(AnonymousType = true)]
    public class vnm_r12oracle_import_servicePreferences
    {
        private string delimiterField;
        private string update_statisticField;
        private string r12_oracle_moduleField;

        /// <remarks />
        [XmlElement("delimiter")]  
        public string delimiter
        {
            get => delimiterField;
            set => delimiterField = value;
        }
        /// <remarks />
        [XmlElement("update_statistic")]  
        public string update_statistic
        {
            get => update_statisticField;
            set => update_statisticField = value;
        }        /// <remarks />
        [XmlElement("r12_oracle_module")]  
        public string r12_oracle_module
        {
            get => r12_oracle_moduleField;
            set => r12_oracle_moduleField = value;
        }
    }
      
    /// <remarks />
    [Serializable]
    [DesignerCategory("code")]
    [XmlTypeAttribute(AnonymousType = true)]
    public class vnm_r12oracle_import_serviceService
    {
        private string pathField;
        private string portField;
        private string auth_token_headerField;
        private string auth_tokenField;

        /// <remarks />
        [XmlElement("path")]
        public string path
        {
            get => pathField;
            set => pathField = value;
        }
        /// <remarks />
        [XmlElement("port")]
        public string port
        {
            get => portField;
            set => portField = value;
        }
        /// <remarks />
        [XmlElement("auth_token_header")]
        public string auth_token_header
        {
            get => auth_token_headerField;
            set => auth_token_headerField = value;
        }
        /// <remarks />
        [XmlElement("auth_token")]
        public string auth_token
        {
            get => auth_tokenField;
            set => auth_tokenField = value;
        }
    }
    

    /// <remarks />
    [Serializable]
    [DesignerCategory("code")]
    [XmlTypeAttribute(AnonymousType = true)]
    public class vnm_r12oracle_import_serviceConnection
    {
        private string idField;
        private string typeField;
        private string driverClassNameField;
        private string locationField;
        private string hostField;
        private string portField;
        private string service_nameField;
        private string auth_tokenField;
        private string check_methodField;

        // private string auth_tokenField;
        // private string driverClassNameField;
        // private string idField;
        // private string locationField;
        // private string typeField;
        /// <remarks />
        
        /// <remarks />
        [XmlElement("id")]
        public string id
        {
            get => idField;
            set => idField = value;
        }
        /// <remarks />
        [XmlElement("type")]
        public string type
        {
            get => typeField;
            set => typeField = value;
        }
        /// <remarks />
        [XmlElement("driverClassName")]
        public string driverClassName
        {
            get => driverClassNameField;
            set => driverClassNameField = value;
        }
        /// <remarks />
        [XmlElement("location")]
        public string location
        {
            get => locationField;
            set => locationField = value;
        }

        /// <remarks />
        [XmlElement("host")]
        public string host
        {
            get => hostField;
            set => hostField = value;
        }
        /// <remarks />
        [XmlElement("port")]
        public string port
        {
            get => portField;
            set => portField = value;
        }
        /// <remarks />
        [XmlElement("service_name")]
        public string service_name
        {
            get => service_nameField;
            set => service_nameField = value;
        }
        /// <remarks />        
        [XmlElement("auth_token")]
        public string auth_token
        {
            get => auth_tokenField;
            set => auth_tokenField = value;
        }
        /// <remarks />
        [XmlElement("check_method")]
        public string check_method
        {
            get => check_methodField;
            set => check_methodField = value;
        }
    }

    /// <remarks />
    [Serializable]
    [DesignerCategory("code")]
    [XmlTypeAttribute(AnonymousType = true)]
    public class vnm_r12oracle_import_serviceFlow
    {
        private string nameField;
        private string type_flowField;
        private string taxpackField;
        private string taxformField;
        private string moduleField;
        private string queue_connectionField;
        private string queue_tableField;
        private string source_connectionField;
        private string target_connectionField;
        private string calculation_delta_valField;
        private string source_query_textField;
        private string target_tableField;
        private string update_targetField;

        /// <remarks />
        [XmlElement("name")]
        public string name
        {
            get => nameField;
            set => nameField = value;
        }

        /// <remarks />
        [XmlElement("type_flow")]
        public string type_flow
        {
            get => type_flowField;
            set => type_flowField = value;
        }

        /// <remarks />
        [XmlElement("taxpack")]
        public string taxpack
        {
            get => taxpackField;
            set => taxpackField = value;
        }

        /// <remarks />
        [XmlElement("taxform")]
        public string taxform
        {
            get => taxformField;
            set => taxformField = value;
        }

        /// <remarks />
        [XmlElement("module")]
        public string module
        {
            get => moduleField;
            set => moduleField = value;
        }

        /// <remarks />
        [XmlElement("queue_connection")]
        public string queue_connection
        {
            get => queue_connectionField;
            set => queue_connectionField = value;
        }

        /// <remarks />
        [XmlElement("queue_table")]
        public string queue_table
        {
            get => queue_tableField;
            set => queue_tableField = value;
        }

        /// <remarks />
        [XmlElement("source_connection")]
        public string source_connection
        {
            get => source_connectionField;
            set => source_connectionField = value;
        }

        /// <remarks />
        [XmlElement("target_connection")]
        public string target_connection
        {
            get => target_connectionField;
            set => target_connectionField = value;
        }

        /// <remarks />
        [XmlElement("calculation_delta_val")]
        public string calculation_delta_val
        {
            get => calculation_delta_valField;
            set => calculation_delta_valField = value;
        }

        /// <remarks />
        [XmlElement("source_query_text")]
        public string source_query_text
        {
            get => source_query_textField;
            set => source_query_textField = value;
        }

        /// <remarks />
        [XmlElement("target_table")]
        public string target_table
        {
            get => target_tableField;
            set => target_tableField = value;
        }

        /// <remarks />
        [XmlElement("update_target")]
        public string update_target
        {
            get => update_targetField;
            set => update_targetField = value;
        }
    }

}